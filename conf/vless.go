package conf

import "encoding/json"

type VlessAccount struct {
	ID         string `json:"id"`
	Level      uint16 `json:"level,omitempty"`
	Encryption string `json:"encryption"`
}

type VlessOutboundTarget struct {
	Address string         `json:"address"`
	Users   []VlessAccount `json:"users"`
	Level   uint16         `json:"level"`
}

type VlessOutboundConfig struct {
	Receivers []*VlessOutboundTarget `json:"vnext"`
}

func NewVlessOutboundConfig(jsonData json.RawMessage) (*VlessOutboundConfig, error) {
	conf := &VlessOutboundConfig{}
	return conf, json.Unmarshal(jsonData, conf)
}
