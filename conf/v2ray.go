package conf

import (
	"encoding/json"
	"os"
)

type OutboundConfig struct {
	Protocol      string          `json:"protocol"`
	Tag           string          `json:"tag,omitempty"`
	Settings      json.RawMessage `json:"settings,omitempty"`
	StreamSetting *StreamConfig   `json:"streamSettings,omitempty"`
	MuxSettings   *MuxConfig      `json:"mux,omitempty"`
}

type InboundConfig struct {
	Port           *uint           `json:"port"`
	Protocol       string          `json:"protocol"`
	StreamSetting  *StreamConfig   `json:"streamSettings,omitempty"`
	Settings       json.RawMessage `json:"settings,omitempty"`
	Tag            string          `json:"tag,omitempty"`
	DomainOverride *[]string       `json:"domainOverride,omitempty"`
	Sniffing       *SniffingConfig `json:"sniffing,omitempty"`
}

type SniffingConfig struct {
	Enabled      bool     `json:"enabled"`
	DestOverride []string `json:"destOverride"`
}

type SocketConfig struct {
	Mark   int32  `json:"mark,omitempty"`
	TFO    *bool  `json:"tcpFastOpen,omitempty"`
	TProxy string `json:"tproxy,omitempty"`
}

type MuxConfig struct {
	Enabled     bool   `json:"enabled,omitempty"`
	Concurrency uint16 `json:"concurrency,omitempty"`
}

type V2rayConfig struct {
	Port               uint16            `json:"port,omitempty"`
	RouterConfig       *RouterConfig     `json:"routing"`
	DNSConfig          *DnsConfig        `json:"dns"`
	InboundConfigList  []*InboundConfig  `json:"inbounds"`
	OutboundConfigList []*OutboundConfig `json:"outbounds,omitempty"`
}

func ParseV2ray(conf string) (*V2rayConfig, error) {
	confJson, err := os.Open(conf)
	if err != nil {
		return nil, err
	}

	v2rayConfig := new(V2rayConfig)
	if err = json.NewDecoder(confJson).Decode(v2rayConfig); err != nil {
		return nil, err
	}

	return v2rayConfig, nil
}

func (v2ray *V2rayConfig) Save(conf string) error {
	confJson, err := os.OpenFile(conf, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		return err
	}

	return json.NewEncoder(confJson).Encode(v2ray)
}

func NewV2rayConfig() *V2rayConfig {
	v2ray := &V2rayConfig{}

	tproxy := Conf.TProxy
	if tproxy == "" {
		tproxy = "redirect"
	}

	port := uint(Conf.ListerPort)
	dokodemoConfig := &InboundConfig{Port: &port}
	dokodemoConfig.DomainOverride = &[]string{"tls", "http"}
	dokodemoConfig.Protocol = "dokodemo-door"
	dokodemoConfig.Settings = []byte(`{"network": "tcp,udp","followRedirect": true}`)
	dokodemoConfig.Sniffing = &SniffingConfig{true, []string{"http", "tls"}}
	dokodemoConfig.StreamSetting = &StreamConfig{SocketSettings: &SocketConfig{TProxy: tproxy}}

	freedomConfig := &OutboundConfig{}
	freedomConfig.Protocol = "freedom"
	freedomConfig.Tag = "direct"
	freedomConfig.StreamSetting = &StreamConfig{SocketSettings: &SocketConfig{Mark: 255}}

	blackholeConfig := &OutboundConfig{}
	blackholeConfig.Protocol = "blackhole"
	blackholeConfig.Tag = "block"
	blackholeConfig.Settings = []byte(`{"response": {"type": "http"}}`)

	adsRule := RuleConfig{Type: "field", Domain: []string{"geosite:category-ads-all"}, OutboundTag: Conf.AdsOutboundTag}
	btRule := RuleConfig{Type: "field", Protocol: []string{"bittorrent"}, OutboundTag: Conf.BTOutboundTag}
	ipRule := RuleConfig{Type: "field", IP: []string{"geoip:cn", "geoip:private"}, OutboundTag: Conf.IPOutboundTag}
	siteRule := RuleConfig{Type: "field", Domain: []string{"geosite:cn"}, OutboundTag: Conf.SiteOutboundTag}

	whitelistRule := RuleConfig{Type: "field", Domain: Conf.DomainWhitelist, IP: Conf.IPWhitelist, OutboundTag: "direct"}
	blacklistRule := RuleConfig{Type: "field", Domain: Conf.DomainBlacklist, IP: Conf.IPBlacklist, OutboundTag: "proxy"}

	routerConfig := &RouterConfig{}
	routerConfig.Settings = &RouterRulesConfig{DomainStrategy: "IPIfNonMatch", RuleList: []RuleConfig{ipRule, siteRule, adsRule, btRule, whitelistRule, blacklistRule}}

	serverOutboundConfig := &OutboundConfig{Tag: "proxy", Settings: []byte("{}"), StreamSetting: &StreamConfig{SocketSettings: &SocketConfig{Mark: 255}}}

	v2ray.DNSConfig = &DnsConfig{Servers: Conf.DnsServers}
	v2ray.InboundConfigList = append(v2ray.InboundConfigList, dokodemoConfig)
	v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, serverOutboundConfig)
	v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, freedomConfig)
	v2ray.OutboundConfigList = append(v2ray.OutboundConfigList, blackholeConfig)
	v2ray.RouterConfig = routerConfig

	return v2ray
}
