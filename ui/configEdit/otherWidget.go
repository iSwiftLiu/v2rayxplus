package configEdit

import (
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"strconv"
)

type OtherWidget struct {
	*widgets.QWidget

	dnsLineWidget  *widgets.QListWidget
	tproxyLineEdit *widgets.QLineEdit
	portLineEdit   *widgets.QLineEdit
	adsLineEdit    *widgets.QLineEdit
	btLineEdit     *widgets.QLineEdit
	ipLineEdit     *widgets.QLineEdit
	siteLineEdit   *widgets.QLineEdit

	reapplyButton *widgets.QPushButton

	config *conf.V2rayConfig
}

func NewOtherWidget(parent widgets.QWidget_ITF) *OtherWidget {
	widget := widgets.NewQWidget(parent, 0)

	otherWidget := &OtherWidget{QWidget: widget}
	otherWidget.init()
	otherWidget.initConnect()

	return otherWidget
}

func (ptr *OtherWidget) init() {
	formLayout := widgets.NewQFormLayout(ptr)

	ptr.dnsLineWidget = widgets.NewQListWidget(ptr)
	ptr.dnsLineWidget.SetEnabled(false)

	ptr.tproxyLineEdit = widgets.NewQLineEdit(ptr)
	ptr.portLineEdit = widgets.NewQLineEdit(ptr)
	ptr.adsLineEdit = widgets.NewQLineEdit(ptr)
	ptr.btLineEdit = widgets.NewQLineEdit(ptr)
	ptr.ipLineEdit = widgets.NewQLineEdit(ptr)
	ptr.siteLineEdit = widgets.NewQLineEdit(ptr)

	ptr.tproxyLineEdit.SetEnabled(false)
	ptr.portLineEdit.SetEnabled(false)
	ptr.adsLineEdit.SetEnabled(false)
	ptr.btLineEdit.SetEnabled(false)
	ptr.ipLineEdit.SetEnabled(false)
	ptr.siteLineEdit.SetEnabled(false)

	ptr.reapplyButton = widgets.NewQPushButton2("重新应用设置中的配置", ptr)

	formLayout.AddRow3("DNS", ptr.dnsLineWidget)
	formLayout.AddRow3("透明代理方式", ptr.tproxyLineEdit)
	formLayout.AddRow3("透明代理监听端口", ptr.portLineEdit)
	formLayout.AddRow3("广告分流", ptr.adsLineEdit)
	formLayout.AddRow3("BT 分流", ptr.btLineEdit)
	formLayout.AddRow3("国内与私有IP分流", ptr.ipLineEdit)
	formLayout.AddRow3("国内域名分流", ptr.siteLineEdit)
	formLayout.AddRow5(ptr.reapplyButton)

	ptr.SetLayout(formLayout)
}

func (ptr *OtherWidget) initConnect() {
	ptr.reapplyButton.ConnectClicked(func(checked bool) {
		ptr.config.DNSConfig.Servers = conf.Conf.DnsServers

		for _, inboundConfig := range ptr.config.InboundConfigList {
			if inboundConfig.Protocol == "dokodemo-door" {
				if inboundConfig.StreamSetting != nil && inboundConfig.StreamSetting.SocketSettings != nil {
					inboundConfig.StreamSetting.SocketSettings.TProxy = conf.Conf.TProxy
				}
				port := uint(conf.Conf.ListerPort)
				inboundConfig.Port = &port
			}
		}

		adsExist, btExist, ipExist, siteExist := false, false, false, false

		for _, rule := range ptr.config.RouterConfig.Settings.RuleList {
			for _, protocol := range rule.Protocol {
				if protocol == "bittorrent" {
					rule.OutboundTag = conf.Conf.BTOutboundTag
					btExist = true
					break
				}
			}

			for _, ip := range rule.IP {
				if ip == "geoip:cn" {
					rule.OutboundTag = conf.Conf.IPOutboundTag
					ipExist = true
					break
				}
			}

			for _, domain := range rule.Domain {
				if domain == "geosite:category-ads-all" {
					rule.OutboundTag = conf.Conf.AdsOutboundTag
					adsExist = true
					break
				} else if domain == "geosite:cn" {
					rule.OutboundTag = conf.Conf.SiteOutboundTag
					siteExist = true
					break
				}
			}
		}

		if !ipExist {
			ptr.config.RouterConfig.Settings.RuleList =
				append(ptr.config.RouterConfig.Settings.RuleList,
					conf.RuleConfig{Type: "field", IP: []string{"geoip:cn", "geoip:private"}, OutboundTag: conf.Conf.IPOutboundTag})
		}

		if !btExist {
			ptr.config.RouterConfig.Settings.RuleList =
				append(ptr.config.RouterConfig.Settings.RuleList,
					conf.RuleConfig{Type: "field", Protocol: []string{"bittorrent"}, OutboundTag: conf.Conf.BTOutboundTag})
		}

		if !siteExist {
			ptr.config.RouterConfig.Settings.RuleList =
				append(ptr.config.RouterConfig.Settings.RuleList,
					conf.RuleConfig{Type: "field", Domain: []string{"geosite:cn"}, OutboundTag: conf.Conf.SiteOutboundTag})
		}

		if !adsExist {
			ptr.config.RouterConfig.Settings.RuleList =
				append(ptr.config.RouterConfig.Settings.RuleList,
					conf.RuleConfig{Type: "field", Domain: []string{"geosite:category-ads-all"}, OutboundTag: conf.Conf.AdsOutboundTag})
		}

		ptr.parse()
	})
}

func (ptr *OtherWidget) parseConfig(config *conf.V2rayConfig) {
	ptr.config = config

	ptr.parse()
}

func (ptr *OtherWidget) parse() {
	ptr.dnsLineWidget.Clear()
	ptr.dnsLineWidget.AddItems(ptr.config.DNSConfig.Servers)

	for _, inboundConfig := range ptr.config.InboundConfigList {
		if inboundConfig.Protocol == "dokodemo-door" {
			if inboundConfig.StreamSetting != nil && inboundConfig.StreamSetting.SocketSettings != nil {
				ptr.tproxyLineEdit.SetText(inboundConfig.StreamSetting.SocketSettings.TProxy)
			}
			ptr.portLineEdit.SetText(strconv.Itoa(int(*inboundConfig.Port)))
		}
	}

	for _, rule := range ptr.config.RouterConfig.Settings.RuleList {
		for _, protocol := range rule.Protocol {
			if protocol == "bittorrent" {
				ptr.btLineEdit.SetText(rule.OutboundTag)
				break
			}
		}

		for _, ip := range rule.IP {
			if ip == "geoip:cn" {
				ptr.ipLineEdit.SetText(rule.OutboundTag)
				break
			}
		}

		for _, domain := range rule.Domain {
			if domain == "geosite:category-ads-all" {
				ptr.adsLineEdit.SetText(rule.OutboundTag)
				break
			} else if domain == "geosite:cn" {
				ptr.siteLineEdit.SetText(rule.OutboundTag)
				break
			}
		}
	}
}

func (ptr *OtherWidget) saveConfig() {

}

func (ptr *OtherWidget) stringToUint(s string) uint {
	if i, err := strconv.Atoi(s); err == nil {
		return uint(i)
	} else {
		return 0
	}
}
