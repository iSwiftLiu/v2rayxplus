package configEdit

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"strconv"
)

type VlessConfig struct {
	*widgets.QFrame

	formLayout *widgets.QFormLayout

	uuidLineEdit       *widgets.QLineEdit
	encryptionLineEdit *widgets.QLineEdit
	levelLineEdit      *widgets.QLineEdit
}

func NewVlessConfig(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *VlessConfig {
	frame := widgets.NewQFrame(parent, fo)

	vlessConfig := &VlessConfig{QFrame: frame}
	vlessConfig.init()

	return vlessConfig
}

func (ptr *VlessConfig) init() {
	ptr.formLayout = widgets.NewQFormLayout(ptr)
	ptr.formLayout.SetContentsMargins(0, 0, 0, 0)

	ptr.uuidLineEdit = widgets.NewQLineEdit(ptr)
	ptr.encryptionLineEdit = widgets.NewQLineEdit2("none", ptr)
	ptr.levelLineEdit = widgets.NewQLineEdit2("0", ptr)

	ptr.formLayout.AddRow3("id", ptr.uuidLineEdit)
	ptr.formLayout.AddRow3("encryption", ptr.encryptionLineEdit)
	ptr.formLayout.AddRow3("level", ptr.levelLineEdit)

	ptr.SetLayout(ptr.formLayout)
}

func (ptr *VlessConfig) ParseConf(config *conf.VlessOutboundTarget) {
	if len(config.Users) > 0 {
		ptr.uuidLineEdit.SetText(config.Users[0].ID)
		level := strconv.FormatUint(uint64(config.Users[0].Level), 10)
		ptr.levelLineEdit.SetText(level)
		if config.Users[0].Encryption != "" {
			ptr.encryptionLineEdit.SetText(config.Users[0].Encryption)
		}
	}
}

func (ptr *VlessConfig) SaveConf(config *conf.VlessOutboundTarget) {
	if len(config.Users) == 0 {
		config.Users = append(config.Users, conf.VlessAccount{})
	}
	config.Users[0].ID = ptr.uuidLineEdit.Text()
	level, _ := strconv.ParseUint(ptr.levelLineEdit.Text(), 10, 0)
	config.Users[0].Level = uint16(level)
	if ptr.encryptionLineEdit.Text() == "" {
		config.Users[0].Encryption = "none"
	} else {
		config.Users[0].Encryption = ptr.encryptionLineEdit.Text()
	}
}
