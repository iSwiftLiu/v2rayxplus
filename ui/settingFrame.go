package ui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"strconv"
	"strings"
	"unicode"
)

var outboundTag = []string{"direct", "proxy", "block"}

type SettingFrame struct {
	*widgets.QFrame

	themeComboBox  *widgets.QComboBox
	tproxyComboBox *widgets.QComboBox

	adsComboBox  *widgets.QComboBox
	btComboBox   *widgets.QComboBox
	ipComboBox   *widgets.QComboBox
	siteComboBox *widgets.QComboBox

	portEdit *widgets.QLineEdit
	dnsEdit  *widgets.QPlainTextEdit

	domainWhitelistEdit *widgets.QPlainTextEdit
	domainBlacklistEdit *widgets.QPlainTextEdit

	ipWhitelistEdit *widgets.QPlainTextEdit
	ipBlacklistEdit *widgets.QPlainTextEdit

	saveButton   *widgets.QPushButton
	cancelButton *widgets.QPushButton
}

func NewSettingFrame(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *SettingFrame {
	frame := widgets.NewQFrame(parent, fo)

	settingFrame := &SettingFrame{QFrame: frame}
	settingFrame.init()
	settingFrame.initConnect()

	return settingFrame
}

func (ptr *SettingFrame) init() {
	ptr.SetWindowTitle("设置")

	vboxLayout := widgets.NewQVBoxLayout2(ptr)

	baseLabel := widgets.NewQLabel2("基本", ptr, 0)

	scrollArea := widgets.NewQScrollArea(ptr)
	scrollLayout := widgets.NewQFormLayout(scrollArea)

	ptr.themeComboBox = widgets.NewQComboBox(ptr)
	ptr.themeComboBox.AddItems([]string{"light", "dark"})
	ptr.themeComboBox.SetCurrentText(conf.Conf.Theme)

	ptr.tproxyComboBox = widgets.NewQComboBox(ptr)
	ptr.tproxyComboBox.AddItems([]string{"redirect", "tproxy"})
	ptr.tproxyComboBox.SetCurrentText(conf.Conf.TProxy)

	ptr.portEdit = widgets.NewQLineEdit2(strconv.FormatInt(int64(conf.Conf.ListerPort), 10), ptr)
	ptr.portEdit.SetValidator(gui.NewQIntValidator2(1024, 65535, ptr))

	ptr.dnsEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.DnsServers, ",\n"), ptr)
	ptr.dnsEdit.SetFixedHeight(72)

	scrollLayout.AddRow3("主题:", ptr.themeComboBox)
	scrollLayout.AddRow3("透明代理方式:", ptr.tproxyComboBox)
	scrollLayout.AddRow3("监听端口:", ptr.portEdit)
	scrollLayout.AddRow3("DNS服务器:", ptr.dnsEdit)

	routerLabel := widgets.NewQLabel2("路由", ptr, 0)
	routerScrollArea := widgets.NewQScrollArea(ptr)
	routerScrollLayout := widgets.NewQFormLayout(routerScrollArea)

	ptr.adsComboBox = widgets.NewQComboBox(ptr)
	ptr.btComboBox = widgets.NewQComboBox(ptr)
	ptr.ipComboBox = widgets.NewQComboBox(ptr)
	ptr.siteComboBox = widgets.NewQComboBox(ptr)

	ptr.adsComboBox.AddItems(outboundTag)
	ptr.btComboBox.AddItems(outboundTag)
	ptr.ipComboBox.AddItems(outboundTag)
	ptr.siteComboBox.AddItems(outboundTag)

	ptr.adsComboBox.SetCurrentText(conf.Conf.AdsOutboundTag)
	ptr.btComboBox.SetCurrentText(conf.Conf.BTOutboundTag)
	ptr.ipComboBox.SetCurrentText(conf.Conf.IPOutboundTag)
	ptr.siteComboBox.SetCurrentText(conf.Conf.SiteOutboundTag)

	ptr.domainWhitelistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.DomainWhitelist, ",\n"), ptr)
	ptr.domainWhitelistEdit.SetMinimumHeight(72)

	ptr.domainBlacklistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.DomainBlacklist, ",\n"), ptr)
	ptr.domainBlacklistEdit.SetMinimumHeight(72)

	ptr.ipWhitelistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.IPWhitelist, ",\n"), ptr)
	ptr.ipWhitelistEdit.SetMinimumHeight(72)

	ptr.ipBlacklistEdit = widgets.NewQPlainTextEdit2(strings.Join(conf.Conf.IPBlacklist, ",\n"), ptr)
	ptr.ipBlacklistEdit.SetMinimumHeight(72)

	routerScrollLayout.AddRow3("广告拦截", ptr.adsComboBox)
	routerScrollLayout.AddRow3("BT 流量", ptr.btComboBox)
	routerScrollLayout.AddRow3("国内IP和私有IP", ptr.ipComboBox)
	routerScrollLayout.AddRow3("国内域名", ptr.siteComboBox)
	routerScrollLayout.AddRow3("域名白名单", ptr.domainWhitelistEdit)
	routerScrollLayout.AddRow3("域名黑名单", ptr.domainBlacklistEdit)
	routerScrollLayout.AddRow3("IP白名单", ptr.ipWhitelistEdit)
	routerScrollLayout.AddRow3("IP黑名单", ptr.ipBlacklistEdit)

	hboxLayout := widgets.NewQHBoxLayout2(ptr)

	ptr.saveButton = widgets.NewQPushButton2("保存", ptr)
	ptr.cancelButton = widgets.NewQPushButton2("取消", ptr)

	hboxLayout.AddStretch(1)
	hboxLayout.AddWidget(ptr.saveButton, 0, core.Qt__AlignRight)
	hboxLayout.AddWidget(ptr.cancelButton, 0, core.Qt__AlignRight)

	vboxLayout.AddWidget(baseLabel, 0, core.Qt__AlignLeft)
	vboxLayout.AddWidget(scrollArea, 1, 0)
	vboxLayout.AddWidget(routerLabel, 0, core.Qt__AlignLeft)
	vboxLayout.AddWidget(routerScrollArea, 1, 0)
	vboxLayout.AddLayout(hboxLayout, 0)

	ptr.SetLayout(vboxLayout)
}

func (ptr *SettingFrame) initConnect() {
	ptr.saveButton.ConnectClicked(ptr.saveButtonClicked)

	ptr.cancelButton.ConnectClicked(func(checked bool) {
		ptr.Close()
	})
}

func (ptr *SettingFrame) saveButtonClicked(checked bool) {
	themeChange := false

	theme := ptr.themeComboBox.CurrentText()
	if conf.Conf.Theme != theme {
		conf.Conf.Theme = theme
		themeChange = true
	}

	conf.Conf.TProxy = ptr.tproxyComboBox.CurrentText()
	conf.Conf.ListerPort, _ = strconv.Atoi(ptr.portEdit.Text())
	conf.Conf.DnsServers = strings.FieldsFunc(strings.ReplaceAll(ptr.dnsEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	conf.Conf.AdsOutboundTag = ptr.adsComboBox.CurrentText()
	conf.Conf.BTOutboundTag = ptr.btComboBox.CurrentText()
	conf.Conf.IPOutboundTag = ptr.ipComboBox.CurrentText()
	conf.Conf.SiteOutboundTag = ptr.siteComboBox.CurrentText()

	conf.Conf.DomainWhitelist = strings.FieldsFunc(strings.ReplaceAll(ptr.domainWhitelistEdit.ToPlainText(), ",", " "), unicode.IsSpace)
	conf.Conf.DomainBlacklist = strings.FieldsFunc(strings.ReplaceAll(ptr.domainBlacklistEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	conf.Conf.IPWhitelist = strings.FieldsFunc(strings.ReplaceAll(ptr.ipWhitelistEdit.ToPlainText(), ",", " "), unicode.IsSpace)
	conf.Conf.IPBlacklist = strings.FieldsFunc(strings.ReplaceAll(ptr.ipBlacklistEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	if err := conf.Conf.SaveConf(); err != nil {
		widgets.QMessageBox_Information(ptr, "错误", "配置文件保存失败，错误："+err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	} else {
		if themeChange {
			widgets.QMessageBox_Information(ptr, "提示", "主题修改将在下次启动时生效", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		}

		ptr.Close()
	}
}
